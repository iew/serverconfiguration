@echo off

start "BUS" "bus\apache-activemq-5.13.2\bin\activemq.bat" start

start "ENTERPRISE" /D enterprise\apache-tomcat-8.0.35\bin "startup.bat"

start "PORTAL" /D portal\liferay-portal-6.1.2-ce-ga3\tomcat-7.0.40\bin "startup.bat"