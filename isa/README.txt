****BUS*********************************************************************
This folder contains ActiveMQ

****Docker******************************************************************
This Folder contains docker files to run the project as docker containers

****ISA*********************************************************************
This folder contains apache tomcat with the wars installed

---Starting-----------------------------------------------------------------
Windows:
	just run the start.bat in the root folder

Linux: 
	first start activemq:
		bus\apache-activemq-5.13.2\bin\activemq start
	then start tomcat:
		isa\apache-tomcat-8.0.35\bin\startup.sh
		
---Configuring--------------------------------------------------------------
RMC:
	edit isa\apache-tomcat-8.0.35\lib\com.omi.web.rmc.properties
	From this you can set the activemq url and the database connection options
TestWarn:
	edit isa\apache-tomcat-8.0.35\lib\testwarn-config.yml
	The only things you should have to change here are the messagecenter url and the plot options
ISA:
	edit isa\apache-tomcat-8.0.35\lib\isa.properties
	Here you can set the controller ip and port as well as configure activemq properties
	
Tomcat:
	/manager login --> U:iew  P:iew123