# README #

These are the components necessary to run the IEW CBRNWatcboard Architecture
Java 1.8
R 3.3.2

The assumed installation directory is:
C:\opt\iew\iew-2.0.0.{version}

### ActiveMQ Bus ###
*Admin http://localhost:8161/admin
*REST http://localhost:8161/api

### Enterprise Tomcat ###
*Site http://localhost:8080/
*Manager http://localhost:8080/manager		user//pass == iew//iew123
*Map http://localhost:8080/com.omi.mapservice.web/
*JMS Tester http://localhost:8080/org.cbrnwatchboard.jms.examples/
*IMPACT http://localhost:8080/gov.sandia.impact/jsp/index.html

### Portal ###
*Site http://localhost:9080/

### Release Notes ###
RC3
===
IMPACT
	Fixes bug with cluster & temporal models running after the anomaly detector triggering for a single day.  The anomaly detector now executes across over multiple days and will allow the cluster/temporal models to run only if it triggers for multiple, consecutive days.  The consecutive day threshold varies depending on the identified disease.
	Updated IMPACT code to save region data from ESSENCE messages.
	Filter IMPACT executions to only consider temporal messages within the “San Diego” region.
	Updated IMPACT alert databases to include region information within each alert.


RC2
===
IMPACT
	Updated temporal inverse models and anomaly detectors to be better tuned for the TD2 data
	Added temporal inverse models for Glanders, Flu, and Tularemia
	Enabled Response Pathways model to provide results on deploying prophylaxis for the diagnosed contagion

TD2
===
RMC index - create index data_piece_struct_index on data_piece_struct_property (data_piece_struct_id);
Map 0.1.15
	Fixed gray button and layer Z-order.
IMPACT-DSS 0.26
	TD2 data injects
IMPACT 0.0.18
	new models
WebRMC 1.0.25 & 2.0.35
	Keystone and bus integration



SPRINT5
=======
Portal
	reset passwords to something other than tacbrd
	Removed old TaCBRD navigation pages that we no longer use
Symbols 0.0.14
	Added more detector symbols
Map 0.1.11
	Added file upload page for whitecell data loading
JMS Tester 0.0.9
	message updates - essence, nbc, cap
WebRMC 2.0.34
	bus integration
	CAP support
TacDOGS 3.0.1.41
	server side updates for new tablet version
IMPACT 0.0.5
	anomaly detectors wired in
	map work
	adapting to ESSENCE message updates


SPRINT4
=======
document the impact config file
TacDOGS war should be named tacdogs.war
tacbrd-symbols - added DEWARN and DEALERT
Add TacDOGS Map Manager to White cell
When deploying TacDOGS war should be named tacdogs##version.war
add map file uploader to whitecell.


SPRINT3
=======
Symbols 0.0.13
	Added images for sensor alerts.	
TacDOGS 3.0.1.0
	create and publish maps	
File Watcher 0.0.2
	Also listens for ECBC sensor alerts and updates map layer
JMS-TESTER 0.0.5
	Added Rob's sensor schema
	Added support for sockjs through SSL
IMPACT 0.0.3
	TacDOGS connection updates.
MAP 0.1.10
	Added SQL support
	Added sockjs support for SSL
	


SPRINT2
=======
Portal
	Added JMS Tester to white cell
	Added Keystone CAP sender to mil-civ
IMPACT 0.0.2
	Receive ESSENCE temporal detection alerts on JMS bus
	Receive TacDOGS sample results on JMS bus
	JSPs refactored to use HTML/Javascript/REST
File Watcher 0.0.1
	Monitors ftp folders for new ESSENCE data and posts to the bus.
	/home/essence/essence.temporal.detection.create
	/home/essence/essence.spatial.detection.create
JMS-TESTER 0.0.3
	Small styling change
	schema updates for env.sample messages
MAP 0.1.9
	Updating proxy settings to help with BSVE integration
KEYSTONE 7
	CAP message sender test
TACDOGS 3.0.0.2
	JMS topic integration
	Tomcat support
	PostgreSQL support
	Message tester



SPRINT1
=======
IMPACT 0.0.1
	Removed references to old architecture.  
	Deployed to Tomcat.
MAP 0.1.8
	Websockets with Sockjs
	ActiveMQ integration
	Removed references to old architecture
	GeoJSON support
SYMBOLS 0.0.12
	Added animals
JSM-TESTER 0.0.1
	Handles all current message schemas
	Publishes and subscribes.
	
	
	
###ACCOUNTS###
Administrator Account
login: admin@cbrnwatchboard.org
password: cbrn

User One - Fifteen
login: user1@cbrnwatchboard.org   (1-15)
password: cbrn

J3 Ops/Battle Captain
login: j3bc@cbrnwatchboard.org
password: cbrn

J3 CBRN 
login: j3cbrn@cbrnwatchboard.org
password: cbrn

J4 Med/Log 
login: j4med@cbrnwatchboard.org
password: cbrn

J5 Plans/Ops 
login: j5plans@cbrnwatchboard.org
password: cbrn

Sample planner (1) 
login: sample1@cbrnwatchboard.org
password: cbrn

Sample Planner (2)
login: sample2@cbrnwatchboard.org
password: cbrn

Public Health officer (PHO) 
login: pho@cbrnwatchboard.org
password: cbrn

Epi One
login: epi1@cbrnwatchboard.org
password: cbrn

Epi Two
login: epi2@cbrnwatchboard.org
password: cbrn

Test Account 1
login test1@cbrnwatchboard.org
password: test1

Test Account 2
login test2@cbrnwatchboard.org
password: test2





